#### Naming - pojmenovávání proměnných
Líbí se mi `softConsonantsMatch`, `hardconsonantsMatch`.
Ostatní matche jsou pojmenovány podle implementace (`szxMatch`), a je mi jasné že najít odpovídající významový název
by vyžadovalo mnohem hlubší znalosti lingvistiky (jestli vůbec lingvisti takto kategorie oddělují).

#### matchVariables
Řešení s objektem, obsahujícím jednotlivá pravidla - a pomocí find se nalezne první platné - 
je dobře škálovatelné (dalších 10 pravidel stačí definovat a připsat do `matchVariables` a `changes`),
ale pro spoustu programátorů to ještě může působit dost exoticky, a nebylo by špatné tedy přidat komentář,
třeba nad pravidla:
```
// Rules analyzed from top to bottom, first matching rule applies (basically switch-case)
```
Také bych se zamyslel, jestli by nebylo lepší, aby pravidlo, efekt, (pořadí) byly u sebe,
místo toho aby jedno bylo uvnitř a druhé vně funkce. Ty to ale máš z dobrého důvodu, že se zde aplikují testy na konkrétní slovo.
Mohli bychom se na ty definice koukat jako na malé objekty:
```
[
{
  name: 'alOlElMatch',
  matchRegexpString: '/(al|ol|el)\b/',
  matchTargetFunctionName: 'twoLastLetters'
},
{
  name: 'usAsEsOsMatch',
  matchRegexpString: '/(us|as|es|os)\b/',
  matchTargetFunctionName: 'twoLastLetters'
},
...
]
```
Poznámka: zde je pořadí dáno pořadím v poli. Občas se to dělá tak, že se pořadí (nebo inverzní `priroita`) připisuje do objektu, např. `priority: 1000` - ale pak je třeba tou prioritou dělat sort (a nějak vyřešit, když mají dvě pravidla stejnou prioritu nebo když má pravidlo priroitu nedefinovanou) 

ALE:
 * museli bychom interpretovat `matchRegexpString` ( `const match = new Regexp(matchRegexpString)` to zkonvertuje)
 * museli bychom interpretovat matchTarget - nejsnáz asi objektem v roli mapy str->funkce
```
const matchTargetToFunction = {
 lastLetter: (name) => name.at(-1),
 twoLastLetters: (name) => name.slice(-2)
}
```
to použiješ `matchTargetFunctions["lastLetter"]("Adam")`

### Zjednodušení funkce inflectedNames
Příjde mi, že to že `inflectedNames` bere pole jmen je trochu neintuitivní, a vlastně to nemá výkonnostní přínos, protože uvnitř stejně děláš for cyklus.
Kdybys ji změnil jen na `inflectedName(name)`, mohlo by to být o trošku jednodušší a přehlednější.



Nějaké tipy pro přehlednější javascript:

 * `newName.indexOf(".") !== -1` -> `newName.includes(".")` - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes
 * `names[name][names[name].length - 1]` -> `names[name].at(-1)
 * `handleClick` bych přejmenoval na `process` nebo něco podobného - pojmenování by mělo odpovídat spíš tomu, co funkce vykoná, než co ji spouští.
